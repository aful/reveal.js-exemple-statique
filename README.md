reveal.js-exemple-statique
==========================

Exemple de présentation reveal.js basée uniquement sur des fichiers statiques,
sans aucune application en écoute (sans code serveur).

Utilisation
-----------

    $ npm ci

et ensuite ouvrir le fichier `index.html` avec un navigateur web avec
le support de JavaScript activé.

C'est déployé et testable sur
https://aful.frama.io/reveal.js-exemple-statique


Documentation
-------------

https://github.com/hakimel/reveal.js/
