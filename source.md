
# reveal.js-exemple-statique

Version 2019-08-02 © Aful https://aful.org/
distribué sous licence CC-BY-SA 4.0

Source : https://framagit.org/aful/reveal.js-exemple-statique

---

<!-- .slide: data-background="#ff0000" -->

Exemple de présentation reveal.js écrite en Markdown
et basée uniquement sur des fichiers statiques,
c'est à dire sans aucune application en écoute (sans code serveur).

---

## Des couleurs

Bonne pratique
<!-- .element: class="good" -->

Mauvaise pratique
<!-- .element: class="bad" -->

Bonne pratique (avec effet d'apparition)
<!-- .element: class="fragment good" -->

Mauvaise pratique (avec effet d'apparition)
<!-- .element: class="fragment bad" -->

---

## Du code

Et maintenant du code :

```javascript
let a
let b = 'some string'
```

---

## Une section

Des items :

* <!-- .element: class="fragment" -->
  Item 1

* <!-- .element: class="fragment" -->
  Item 2

* <!-- .element: class="fragment" -->
  Item 3

---

## Une autre section

Des items dans le désordre :

* <!-- .element: class="fragment" data-fragment-index="2" -->
  Item dans le désordre 2

* <!-- .element: class="fragment" data-fragment-index="1" -->
  Item dans le désordre 1

* <!-- .element: class="fragment" data-fragment-index="3" -->
  Item dans le désordre 3

---

## Une section avec des sous-sections

Blablabla

----

### Sous-section 1

Blablabla

----

### Sous-section 2

Blablabla

----

### Sous-section 3

Blablabla

---

<!-- .slide: data-background="./image/bugs.jpg" -->

FIN
